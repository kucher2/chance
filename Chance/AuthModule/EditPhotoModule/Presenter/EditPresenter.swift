//
//  EditPresenter.swift
//  Chance
//
//  Created by Ваге Базикян on 30.11.2020.
//

import Foundation

protocol EditPresenterProtocol: Presenter {
    func back()
    func done()
    func editPhoto()
}
    
class EditPresenter: EditPresenterProtocol {
    private var router: AuthRouter?
    private var output: StatableView?
    
    func back() {
        router?.pop()
    }
    func done() {
        router?.pop()
    }
    func editPhoto() {
        router?.push(vc: .auth(.edit))
    }

    required init(_ router: Router?, output: StatableView?) {
        self.router = router as? AuthRouter
        self.output = output
    }
}
