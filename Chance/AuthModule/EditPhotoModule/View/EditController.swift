//
//  EditController.swift
//  Chance
//
//  Created by Ваге Базикян on 30.11.2020.
//

import UIKit

final class EditController: BaseViewController {
    
    private var presenter: EditPresenterProtocol?
    
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imagePickerController = UIImagePickerController()
        
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        let actionSheet = UIAlertController(title: "Photo source", message: "Choose a source", preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }else {
                print("Camera not availible")
            }
        }))
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    public func setPresenter(_ presenter : Presenter?){
        self.presenter = presenter as? EditPresenterProtocol
    }
    @IBAction func cancelAction(_ sender: UIButton) {
        presenter?.back()
    }
    @IBAction func doneAction(_ sender: UIButton) {
        
        presenter?.done()
    }
    @IBAction func sizeAction(_ sender: UIButton) {
        func editSize (_: Bool) -> Bool {
            
            return true
        }
       
//        self.imagePickerController.allowsEditing = true
    }
    
    
}

extension EditController : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard  let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {return}
        imageView.image =  image
       
        picker.dismiss(animated:  true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil )
    }
    
}
