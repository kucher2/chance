//
//  UserInfoPresenter.swift
//  Chance
//
//  Created by Ваге Базикян on 26.11.2020.
//

import Foundation
    
protocol UserPresenterProtocol: Presenter {
    func back()
    func editPhoto()
}
    
class UserPresenter: UserPresenterProtocol {
    private var router: AuthRouter?
    private var output: StatableView?
    
    func back() {
        router?.pop()
    }
    func editPhoto() {
        router?.push(vc: .auth(.edit))
        
    }

    required init(_ router: Router?, output: StatableView?) {
        self.router = router as? AuthRouter
        self.output = output
    }
}
