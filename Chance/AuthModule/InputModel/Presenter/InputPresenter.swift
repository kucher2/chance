//
//  InputPresenter.swift
//  Chance
//
//  Created by Ваге Базикян on 30.11.2020.
//

import Foundation


protocol InputPresenetProtocol: Presenter {
    func signIn()
    func back()
    func nextUser()
}

class InputPresenet: InputPresenetProtocol {
    private var router: AuthRouter?
    private var output:  StatableView?
    
    func signIn() {
        router?.push(vc: .auth(.signIn))
        output?.updateState(s: .loading)
    }
    func back() {
        router?.pop()
    }
    func nextUser() {
        router?.push(vc: .auth(.nextUser))
    }
    
  
    
    required init(_ router: Router?, output: StatableView?) {
        self.router = router as? AuthRouter
        self.output = output
    }
}
