//
//  InputController.swift
//  Chance
//
//  Created by Ваге Базикян on 27.11.2020.
//

import UIKit

final class InputController: BaseViewController {
    
    private var presenter: InputPresenetProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        }

    public func setPresenter( _ presenter: Presenter?) {
        self.presenter = presenter as? InputPresenetProtocol
    }
    
    
    
    @IBAction func backAction(_ sender: UIButton) {
        presenter?.back()
    }
    @IBAction func nextAction(_ sender: UIButton) {
        presenter?.nextUser()
    }
}
